import { createStore } from "redux";
import { rootReducer } from "./rootReducer";

export const store = createStore(
  rootReducer,
  window.REDUX_DEVTOOLS_EXTENSION && window.REDUX_DEVTOOLS_EXTENSION()
);
