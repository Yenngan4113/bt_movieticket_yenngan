import { combineReducers } from "redux";
import { ticketReducer } from "./ticketReducer.js";

export const rootReducer = combineReducers({
  ticketReducer,
});
