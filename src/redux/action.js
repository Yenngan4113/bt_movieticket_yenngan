import { ADD_TICKET, DELETE_TICKET } from "./constant";

export const addItemAction = (payload) => {
  return {
    type: ADD_TICKET,
    payload,
  };
};
export const deleteItemAction = (payload) => {
  return {
    type: DELETE_TICKET,
    payload,
  };
};
