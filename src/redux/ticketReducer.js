import { data } from "../component/data";
import { ADD_TICKET, DELETE_TICKET } from "./constant";

let initialState = {
  seatList: [...data],
  cart: [],
  isActive: false,
};

export const ticketReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case ADD_TICKET: {
      let cloneCart = [...state.cart];
      let index = cloneCart.findIndex((item) => {
        return item.soGhe == payload.soGhe;
      });

      if (index == -1) {
        let newItem = { ...payload, quantity: 1 };

        cloneCart.push(newItem);
      }
      if (index != -1) {
        cloneCart.splice(index, 1);
        state.isActive = false;
      }
      state.cart = cloneCart;
      let dataJson = JSON.stringify(state);
      let dataState = JSON.parse(dataJson);
      return { ...dataState };
    }
    case DELETE_TICKET: {
      let cloneCart = [...state.cart];
      let index = cloneCart.findIndex((cartItem) => {
        return cartItem.soGhe == payload.soGhe;
      });
      if (index != -1) {
        cloneCart.splice(index, 1);
      }
      state.cart = cloneCart;
      let dataJson = JSON.stringify(state);
      let dataState = JSON.parse(dataJson);
      return { ...dataState };
    }
    default:
      return state;
  }
};
