import React, { Component } from "react";
import { connect } from "react-redux";
import TicketItem from "./TicketItem";

class TicketList extends Component {
  render() {
    return (
      <div>
        <table className="table">
          <thead>
            <tr className="text-center">
              <td></td>
              <td>1</td>
              <td>2</td>
              <td>3</td>
              <td>4</td>
              <td>5</td>
              <td>6</td>
              <td>7</td>
              <td>8</td>
              <td>9</td>
              <td>10</td>
              <td>11</td>
              <td>12</td>
            </tr>
          </thead>
          {this.props.data.map((item) => {
            return <TicketItem Item={item} />;
          })}
        </table>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return { data: state.ticketReducer.seatList };
};
export default connect(mapStateToProps)(TicketList);
