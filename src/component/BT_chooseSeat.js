import React, { Component } from "react";
import ChosenTicket from "./ChosenTicket";
import TicketList from "./TicketList";

export default class BT_chooseSeat extends Component {
  render() {
    return (
      <div>
        <h1 className="text-center bg-dark text-white">Book movie ticket</h1>
        <div className="row">
          <div className="col-8">
            {" "}
            <TicketList />{" "}
          </div>
          <div className="col-4">
            <ChosenTicket />
          </div>
        </div>
      </div>
    );
  }
}
