import React, { Component, Fragment } from "react";
import { connect } from "react-redux";
import { addItemAction } from "../redux/action";

class TicketItem extends Component {
  render() {
    let { hang } = this.props.Item;
    let { danhSachGhe } = this.props.Item;
    return (
      <Fragment>
        <tbody>
          <tr className="text-center">
            <td className="">{hang}</td>
            {danhSachGhe.map((item) => {
              return (
                <td>
                  <button
                    className={
                      `btn border-0 ` +
                      (item.daDat ? "bg-warning text-danger" : null) +
                      (item.selected ? "bg-success" : null)
                    }
                    style={{ backgroundColor: "#858585" }}
                    id={`${item.soGhe}`}
                    disabled={item.daDat ? "disable" : false}
                    onClick={(e) => {
                      let index = this.props.Selected.findIndex((cartItem) => {
                        return cartItem.soGhe == item.soGhe;
                      });
                      if (index == -1) {
                        e.target.style.backgroundColor = "#08AD00";
                      } else {
                        e.target.style.backgroundColor = "#858585";
                      }
                      this.props.handleAddItem(item);
                    }}
                  >
                    {item.soGhe.substr(1, 2)}
                  </button>
                </td>
              );
            })}
          </tr>
        </tbody>
      </Fragment>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    Selected: state.ticketReducer.cart,
  };
};
const mapDispatchToProps = (dispatch) => {
  return {
    handleAddItem: (item) => {
      dispatch(addItemAction(item));
    },
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(TicketItem);
