import React, { Component } from "react";
import { connect } from "react-redux";
import { deleteItemAction } from "../redux/action";

class ChosenTicket extends Component {
  render() {
    let total = this.props.chosenItem.reduce((acc, currentValue) => {
      return acc + currentValue.gia;
    }, 0);

    return (
      <div>
        <h2> Danh sách ghế đã chọn</h2>
        <div className="d-flex align-items-center mb-2">
          {" "}
          <div
            style={{ height: "30px", width: "30px", display: "inline-block" }}
            className="bg-warning mr-2"
          ></div>
          Ghế đã chọn
        </div>
        <div className="d-flex align-items-center mb-2">
          {" "}
          <div
            style={{ height: "30px", width: "30px", display: "inline-block" }}
            className="bg-success mr-2"
          ></div>
          Ghế đang chọn
        </div>
        <div className="d-flex align-items-center mb-2">
          {" "}
          <div
            style={{ height: "30px", width: "30px", display: "inline-block" }}
            className="bg-secondary mr-2"
          ></div>
          Ghế chưa chọn
        </div>
        <table className="table">
          <thead className="text-center">
            <tr>
              <td>Số ghế</td>
              <td>Giá</td>
              <td>Hủy</td>
            </tr>
          </thead>
          <tbody>
            {this.props.chosenItem.map((item) => {
              return (
                <tr className="text-center">
                  <td>{item.soGhe}</td>
                  <td>{new Intl.NumberFormat("de-DE").format(item.gia)} VND</td>
                  <td className="text-danger">
                    <button
                      className="btn text-danger"
                      onClick={() => {
                        document.getElementById(
                          `${item.soGhe}`
                        ).style.backgroundColor = "#858585";
                        this.props.handleCancel(item);
                      }}
                    >
                      X
                    </button>
                  </td>
                </tr>
              );
            })}
            <tr className="text-center">
              <td>Tổng tiền</td>
              <td>{new Intl.NumberFormat("de-DE").format(total)} VND</td>
              <td></td>
            </tr>
          </tbody>
        </table>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return { chosenItem: state.ticketReducer.cart };
};
const mapDispatchToProps = (dispatch) => {
  return {
    handleCancel: (item) => {
      dispatch(deleteItemAction(item));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ChosenTicket);
