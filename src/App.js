import logo from "./logo.svg";
import "./App.css";
import { Fragment } from "react";
import BT_chooseSeat from "./component/BT_chooseSeat";

function App() {
  return (
    <Fragment>
      <BT_chooseSeat />
    </Fragment>
  );
}

export default App;
